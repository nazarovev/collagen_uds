<?php

class shopUdsPluginFrontendCheckCodeController extends waJsonController
{

    public function execute()
    {

        $uds_helper = new shopUdsHelper();

        $identifier = waRequest::get('code');

        if (!$identifier) {
            $this->setError('Произошла ошибка, неверно передан параметр "Код"');
            return;
        }

        shopUdsHelper::sessionClearAll();

        $response = $uds_helper->customerFind($identifier,'code');


        if ($response['status'] != 'ok') {
            $this->setError('Произошла ошибка, возникла проблема с поиском участника UDS');
            return;
        }

        $result = [
            'data' => $response['data'],
            'reload' => true,
        ];

        if ($response['data']['user']['discount_rate'] > 0) {
            shopUdsHelper::sessionSet('discount', $response['data']['user']['discount_rate']);
            // $result['reload'] = true;
        }

        shopUdsHelper::sessionSet('user', $response['data']['user']);
        shopUdsHelper::sessionSet('identifier', $response['data']['identifier']);
        shopUdsHelper::sessionSet('uds_participant', $response['data']['uds_participant']);

        $this->response = $result;
    }

}
