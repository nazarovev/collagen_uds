<?php

class shopUdsPluginFrontendSubstractCancelController extends waJsonController
{

    public function execute()
    {
        // Чистим сессию только от баллов
        shopUdsHelper::sessionRemove('points');
    }

}
