<?php

class shopUdsPluginFrontendSubstractCodeController extends waJsonController
{

    public function execute()
    {
        $uds_helper = new shopUdsHelper();

        $identifier = waRequest::get('code');
        $points = waRequest::get('points');

        if (!$identifier) {
            $this->setError('Ошибка! Неверно передан Код');
            return;
        }

        if (!isset($points) or $points <= 0) {
            $this->setError('Ошибка! Указано неверное количество баллов.');
            return;
        }

        $response = $uds_helper->customerFind($identifier, 'code');

        if ($response['status'] != 'ok') {
            $this->setError('Ошибка! Сбой во время поиска участника с использованием Кода');
            shopUdsHelper::sessionClearAll();
            return;
        }

        if ($points > $response['data']['user']['max_points']) {
            $message = 'Вы пытаетесь применить больше баллов, чем у вас есть. На данный момент у вас '
                . $response['data']['user']['max_points'] . ' доступные баллы.!';

            $this->setError($message);
            return;
        }

        shopUdsHelper::sessionSet('points', $points);

        $this->response = $response;
    }

}
