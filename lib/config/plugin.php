<?php

return array(
    'name' => "UDS",
    'description' => "модульная экосистема для вашего бизнеса",
    'img'=>'img/uds.ico',
    'version' => '1.0.0',
    'vendor' => 'nazarov',
    'frontend' => true,
    'handlers' => array(
        'frontend_cart' => 'handleFrontendCart',
        'frontend_head' => 'frontendHead',

        'order_calculate_discount' => 'orderCalculateDiscount',

        // 'order_action.*' => 'orderAction',
        'order_action.create' => 'orderActionCreate',

        'order_action.complete' => 'orderActionComplete',
        'order_action.dostavlen' => 'orderActionComplete',

        'order_action.delete' => 'orderActionDelete',
    ),
);
