<?php

return array(


    'shop_uds_orders_new' => array(
        'id' => array('int', 11, 'null' => 0, 'autoincrement' => 1),

        'created_at' => ['datetime', 'null' => 0],
        'updated_at' => ['datetime', 'null' => 1],

        // ID Заказа
        'order_id' => array('int', 11, 'null' => 0, 'default' => '0'),
        // UID Пользователя в UDS
        'uds_uid' => array('varchar', 255),
        // Данные о пользователе в uds - в виде JSON-объекта
        'uds_user_json' => array('text'),
        // Скидка, если используется скидка а не бальная система
        'discount' => array('float', '14,2', 'null' => 0, 'default' => '0'),
        // Списываемые баллы за заказ кодом
        'points' => array('float', '14,2', 'null' => 0, 'default' => '0'),
        // Баллы для начисления за заказ кодом
        'points_for_reward' => array('float', '14,2', 'null' => 0, 'default' => '0'),

        // ID Операции продажи в UDS
        'uds_operation_purchase_id' => array('varchar', 255, 'null' => 1),
        // ID Операции возврата в UDS
        'uds_operation_refund_id' => array('varchar', 255, 'null' => 1),
        // ID Операции начисления вознаграждения
        'uds_operation_reward_id' => array('varchar', 255, 'null' => 1),

        // Тип идентификатора
        'identifier_type' => array('varchar', 255),
        // Значение идентификатора
        'identifier' => array('varchar', 255),

        // Статус хуй знает как используется
        'status' => array('int', 11, 'null' => 0, 'default' => '0'),

        // Флаг - значит возвращено
        'refunded' => array('int', 11, 'null' => 1, 'default' => '0'),
        // Флаг - значит покупка совершена
        'purchased' => array('int', 11, 'null' => 1, 'default' => '0'),
        // Флаг - вознаграждение начислено
        'rewarded' => array('int', 11, 'null' => 1, 'default' => '0'),

        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),
);