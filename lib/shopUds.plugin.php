<?php
// TODO: Тут еще "муха не еблась" - надо что-то делать
// TODO: Пересмотреть в соответствии с новыми данными из Handlers плагина

/**
 * Класс Плагина с обработчиками хуков и вспомогательными функциями
 */
class shopUdsPlugin extends shopPlugin
{
    public function getPath($path)
    {
        $plugin_path = wa('shop')->getPlugin('uds')->path;
        return "{$plugin_path}{$path}";
    }

    public function frontendHead()
    {
        if (!$this->getSettings('enabled')) {
            return;
        }

        $this->addCss('css/uds.css');
        $this->addJs('js/jquery.mask.js');
    }

    public function handleFrontendCart()
    {
        if (!$this->getSettings('enabled')) {
            return;
        }

        $view = new waSmarty3View(wa());
        $settings = $this->getSettings();
        $cashback_message = $settings['cashback_message'];

        $session = shopUdsHelper::session();

        $assign_session = $session;

        if (isset($session['user']['cashback']) and $session['user']['cashback'] > 0) {
            $cashback_message_view = str_replace("[cashback-amount]", $session['user']['cashback'], $cashback_message);
            $assign_session['cashback_message'] = $cashback_message_view;
        }

        $view->assign('session', $assign_session);

        return $view->fetch($this->getPath('/templates/Form.html'));
    }


    public function orderCalculateDiscount($params)
    {
        if (!$this->getSettings('enabled')) {
            return;
        }

        $uds_helper = new shopUdsHelper();

        shopUdsHelper::sessionRemove('total_discount');

        $session = shopUdsHelper::session();
        if (!$session['identifier']) {
            shopUdsHelper::sessionClearAll();
            return;
        }

        $pass = false;

        if ($session['identifier']['type'] != 'phone' and $session['identifier']['type'] != 'code') {
            shopUdsHelper::sessionClearAll();
            return;
        }

        $check_uds_participant = $uds_helper->customerFind(
            $session['identifier']['value'],
            $session['identifier']['type']
        );

        shopUdsHelper::dump($check_uds_participant, '$check_uds_participant');

        if ($check_uds_participant['status'] != 'ok') {
            shopUdsHelper::sessionClearAll();
            return;
        }

        if (!$session['discount'] && !$session['points']) {
            return;
        }

        // Проверка на валидность скидки и баллов

        $valid_discount = true;
        $valid_points = true;

        // Если скидка есть  и она больше, чем у пользователя в настройках
        if ($session['discount']) {
            if ($session['discount'] > $check_uds_participant['data']['user']['discount_rate']) {
                $valid_discount = false;
            }
        }

        if ($session['points']) {
            if ($session['points'] > $check_uds_participant['data']['user']['uds_user_max_points']) {
                $valid_points = false;
            }
        }

        if ($valid_discount and $valid_points) {
            $pass = true;
        }

        if (!$pass) {
            shopUdsHelper::sessionClearAll();
            return;
        }

        $uds_discount = $session['discount'] ?? 0;
        $uds_points = $session['points'] ?? 0;
        $add_points = max(0, $uds_points);

        // if ($uds_points > 0) {
        //     $add_points = $uds_points;
        // } else {
        //     $add_points = 0;
        // }

        $total = 0;

        foreach ($params['order']['items'] as $item_id => $item) {
            if ($item['type'] == 'product') {
                $skus_model = new shopProductSkusModel;
                $sku = $skus_model->getSku($item['sku_id']);
                if (!($this->getSettings('ignore_compare_price') && ($sku['compare_price'] - $sku['price']) > 0)) {

                    $price = shop_currency($item['price'], $item['currency'], $params['order']['currency'], false);
                    $total = $total + ($price * $item['quantity']);

                }
            }
        }

        $description = '';
        if ($uds_discount > 0 && $uds_points > 0) {
            $description = 'Скидка от приложения «UDS» - Скидка: ' . $uds_discount . '%, Баллы: -' . $uds_points . ', UID пользователя: ' . $check_uds_participant['user']['user_uid'];
        } else if ($uds_discount > 0 && $uds_points == 0) {
            $description = 'Скидка от приложения «UDS» - Скидка: ' . $uds_discount . '%, UID пользователя: ' . $check_uds_participant['user']['user_uid'];
        } else if ($uds_points > 0 && $uds_discount == 0) {
            $description = 'Скидка от приложения «UDS» - Баллы: -' . $uds_points . ', UID пользователя: ' . $check_uds_participant['user']['user_uid'];
        }

        if ($uds_discount > 0) {
            $discount_math = $total * $uds_discount / 100.00;
        }

        $result = [];

        if (isset($discount_math)) {
            if ($add_points > 0) {
                $result['discount'] = $discount_math + $add_points;
            } else {
                $result['discount'] = $discount_math;
            }
        } else if ($add_points > 0) {
            $result['discount'] = $add_points;
        }

        if ($result['discount'] <= 0) {
            return;
        }

        // if ($out_discount['discount'] > 0) {
        $result['description'] = $description;

        // wa()->getStorage()->set('shop/uds/total_discount', $result['discount']);
        shopUdsHelper::sessionSet('total_discount', $result['discount']);

        return $result;
    }

    // /** Любое действие - совершаемое с заказом */
    // public function orderAction($params, $event_name)
    // {
    //     shopUdsHelper::dump(
    //         ['params' => $params, 'event_name' => $event_name],
    //         'Какое-то действие, совершаемое с заказом',
    //     );
    //
    //     return;
    // }

    /**
     * Действие - совершаемое при создании заказа
     * @param $params
     *  - order_id - ID Заказа в магазине
     * @return bool|void
     * @throws waException
     */
    public function orderActionCreate($params, $event_name)
    {
        shopUdsHelper::log('Побуждающее событие: ' . $event_name);

        if (!$this->getSettings('enabled')) {
            return;
        }

        shopUdsHelper::log('Зашли в создание Заказа в UDS');
        shopUdsHelper::log('ХУЙ Побуждающее событие: ' . $event_name);
        shopUdsHelper::dump($params, 'Входные параметры');

        $session = shopUdsHelper::session();

        shopUdsHelper::dump($session, 'Сессия');

        // $uds_discount = wa()->getStorage()->get('shop/uds/discount');
        // $uds_points = wa()->getStorage()->get('shop/uds/points');
        // $uds_user = wa()->getStorage()->get('shop/uds/user');

        // waLog::log('Этап 2 Загрузили данные из сессии', 'uds_log.log');

        if (!$session['user']) {
            return false;
        }
        if (!$session['identifier']) {
            return false;
        }

        // waLog::log('Этап 3 Проверили пользователя UDS в сессии', 'uds_log.log');
        // waLog::dump($session['user'], 'uds_log.txt');

        $uds_orders_model = new shopUdsOrderModel();
        $uds_helper = new shopUdsHelper();
        $order_totals = shopUdsTotal::orderTotal($params['order_id']);

        shopUdsHelper::dump($order_totals, 'Высчитали orderTotal по order_id');

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['order_id'] = $params['order_id'];
        $data['identifier_type'] = $session['identifier']['type'];
        $data['identifier'] = $session['identifier']['value'];
        $data['uds_uid'] = $session['user']['uid'];
        $data['uds_user_id'] = $session['user']['id'];
        $data['phone'] = $session['user']['phone'];
        $data['points'] = 0;
        $data['cashback_for_reward'] = $session['user']['cashback'];
        $data['points_for_reward'] = (float)($order_totals['total_for_reward'] * ($session['user']['cashback'] / 100));
        $data['discount'] = 0;

        if ($session['points']) {
            $data['points'] = $session['points'];
        }

        if ($session['discount']) {
            $data['discount'] = $session['discount'];
        }

        shopUdsHelper::dump($data, 'Данные для записи в базу');

        $uds_order_id = $uds_orders_model->insert($data);
        $uds_order = $uds_orders_model->getById($uds_order_id);

        shopUdsHelper::dump($uds_order, 'Данные уже из базы');

//        if ($data['points'] > 0) {
//         waLog::log('Этап 6 Создаем факт покупки', 'uds_log.log');
//         waLog::dump($uds_order, 'uds_log.log');

        // $uds_order = $this->uds_order_model->getByField('order_id', $order_id);

        // $order_totals = shopUdsHelperOrderTotal::calc($order_id);
        if (!$order_totals) {
            $uds_helper->order_log_model->insert([
                'order_id' => $params['order_id'],
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: red;">ERROR (UDS) (Регистрация покупки в UDS): Нет информации о товарах внутри заказа, возможна внутренняя ошибка</code>',
            ]);
        }

        $response = null;
        if ($uds_order['identifier_type'] == 'phone') {
            $response = $uds_helper->api->operationCreateByPhone(
                $uds_order['identifier'],
                $order_totals['total'],
                $order_totals['total'],
                0.00,
                (float)$params['order_id'],
                $order_totals['total'],
            );
        } elseif ($uds_order['identifier_type'] == 'code') {
            $response = $uds_helper->api->operationCreateByCode(
                $uds_order['identifier'],
                $order_totals['total'],
                (float)($order_totals['total'] - $uds_order['points']),
                (float)$uds_order['points'],
                (string)$params['order_id'],
                $order_totals['total'],
            );
        }

        if (!$response) {
            shopUdsHelper::log('Response fail');
            // waLog::dump('Response fail', 'uds_log.log');
        }

        shopUdsHelper::dump($response, 'Ответ от API UDS');
        // waLog::dump($response, 'uds_log.log');

        // Если в ответе содержится ошибка, то информация об ошибке добавляется в логи заказа.
        if ($response['status']['code'] != 200) {
            // Add to order logs
            $data_log = array(
                'order_id' => $params['order_id'],
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
            );
            $uds_helper->order_log_model->insert($data_log);
            // Если в ответе нет ошибки, то полученный идентификатор используется для обновления данных в модели заказов. Обновление происходит в зависимости от переданного в запросе параметра $action.
        } else {
            $purchase_id = $response['content']['id'];

            $uds_helper->uds_order_model->updateById($uds_order['id'],
                [
                    'points_for_reward' => $order_totals['total_for_reward'] * ($uds_order['cashback_for_reward'] / 100),
                    'purchased' => 1,
                    'uds_operation_purchase_id' => $purchase_id,
                ]
            );

        }
//        }

        // waLog::log('Этап 7 Создали факт покупки в UDS', 'nazarov_uds_plugin/uds_log.log');


        shopUdsHelper::sessionRemove('discount');
        shopUdsHelper::sessionRemove('points');
        // wa()->getStorage()->set('shop/uds/discount', '');
        // wa()->getStorage()->set('shop/uds/points', '');
        // waLog::log('Этап 8 Обнулили сессию', 'uds_log.log');
    }

    /**
     *
     */
    public function orderActionComplete($params, $event_name)
    {
        shopUdsHelper::log('orderActionComplete');
        shopUdsHelper::log($event_name);
        shopUdsHelper::dump($params);

        if (!$this->getSettings('enabled')) {
            return;
        }

        shopUdsHelper::dump($params, 'Начало завершения заказа в интернет-магазине');

        $uds_order_model = new shopUdsOrderModel();
        $uds_helper = new shopUdsHelper();
        $order_id = $params['order_id'];
        $uds_order = $uds_order_model->getByField('order_id', $order_id);

        shopUdsHelper::dump($uds_order, 'Информация из нашей базы данных');

        if (!$uds_order) {
            return false;
        }

        if ($uds_order['status'] != 0) {
            return false;
        }

        shopUdsHelper::log('Перед уходом в процесс награждения');

        $uds_helper->operationReward($order_id);

        shopUdsHelper::log('После ухода в процесс награждения');

        return true;
    }

    /**
     * @param $params
     *  - order_id
     * @return bool|void
     * @throws waException
     */
    public function orderActionDelete($params, $event_name)
    {
        shopUdsHelper::log('orderActionDelete');

        if (!$this->getSettings('enabled')) {
            return;
        }


//
        $uds_orders_model = new shopUdsOrderModel();
        $uds_helper = new shopUdsHelper();

        $order_id = $params['order_id'];

        shopUdsHelper::log('Перед получением UDS_ORDER из базы');

        $uds_order = $uds_orders_model->getByField('order_id', $order_id);

        shopUdsHelper::dump($uds_order,'Что получили из базы');

        if (!$uds_order) {
            return false;
        }
        // $uds_order_id = $uds_order['id'];


        // TODO: Тут стоит все пересмотреть

        // if (!empty($uds_order['uds_id']) || !empty($uds_order['refund_uds_id'])) {
        shopUdsHelper::dump($uds_order,'Перед возвратом');
        if ($uds_order['rewarded'] != 1 and $uds_order['refunded'] != 1) {
            shopUdsHelper::log('Условия для возврата соблюдены');
            $response = $uds_helper->api->operationRefund($uds_order['uds_operation_purchase_id']);

            if ($response['status']['code'] != 200) {
                $data_log = array(
                    'order_id' => $order_id,
                    'action_id' => '',
                    'datetime' => date('Y-m-d H:i:s'),
                    'before_state_id' => '',
                    'after_state_id' => '',
                    'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
                );
                $uds_helper->order_log_model->insert($data_log);
            } else {
                $data_log = array(
                    'order_id' => $order_id,
                    'action_id' => '',
                    'datetime' => date('Y-m-d H:i:s'),
                    'before_state_id' => '',
                    'after_state_id' => '',
                    'text' => '<code style="color: green;">UDS - Успешно проведена отмена для транзакции: ' . $uds_order['uds_operation_purchase_id'] . '(отмена: '. $response['content']['id'] .') </code>',
                );
                $uds_helper->order_log_model->insert($data_log);
                $this->uds_order_model->updateById($uds_order['id'], [
                    'refunded' => 1,
                    'uds_operation_refund_id' => $response['content']['id']
                ]);
            }

        }
        // }

    }

    // --------------------------------------
    // --------------------------------------
    // TODO: Тут надо поработать
    // private function substractPoints($data)
    // {
    //
    // }

}
