<?php

/**
 * Класс-помошник, для расчета сумм на которые начисляется скидка или вознаграждение
 */
class shopUdsTotal
{

    static public function calc($items)
    {
        $shopProductParamsModel = new shopProductParamsModel();

        $total = 0;
        $total_for_reward = 0;
        $total_for_discount = 0;

        foreach ($items as $item) {
            $product_id = $item['product_id'];
            $product_price = $item['quantity'] * $item['price'];

            $total = $total + $product_price;

            $params = $shopProductParamsModel->get($product_id);

            if (!isset($params['udsnobonus']) or $params['udsnobonus'] != '1') {
                $total_for_reward = $total_for_reward + $product_price;
            }

            if (!isset($params['udsloyaltyskip']) or $params['udsloyaltyskip'] != '1') {
                $total_for_discount = $total_for_discount + $product_price;
            }
        }

        $skip_loyalty_total = (float)$total - (float)$total_for_discount;

        return [
            'total' => (float)max(0, $total),
            'total_for_reward' => (float)max(0, $total_for_reward),
            'total_for_discount' => (float)max(0, $total_for_discount),
            'skip_loyalty_total' => (float)max(0,$skip_loyalty_total),
        ];
    }

    static public function cartTotal()
    {
        $cart = new shopCart();
        $items = $cart->items();

        return self::calc($items);
    }

    static public function orderTotal($order_id)
    {
        $shopOrderItemsModel = new shopOrderItemsModel();
        $items = $shopOrderItemsModel->getItems($order_id);

        return self::calc($items);
    }

}