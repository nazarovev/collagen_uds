<?php

class shopUdsHelper
{
    /**
     * ------------------------------------------------------------------
     * -- Стартовые парамерты для нестатического использования ----------
     * ------------------------------------------------------------------
     */

    /** Инициализируемый объект класса для работы с API UDS */
    protected $api;
    /** Настройки(параметры) компании в системе UDS - Беруться из API */
    protected $udsCompanySettings;
    /** Настройки плагина  UDS */
    protected $pluginSettings;
    /** Модель заказов, Модель заказов UDS, Модель лога заказов */
    protected $order_model, $uds_order_model, $order_log_model;

    /** Конструктор */
    public function __construct()
    {
        $plugin = wa('shop')->getPlugin('uds');
        $this->pluginSettings = $plugin->getSettings();

        $this->api = new shopUdsApi($this->pluginSettings['api_key'], $this->pluginSettings['company_id']);
        $this->udsCompanySettings = $this->api->settings();

        $this->order_model = new shopOrderModel();
        $this->uds_order_model = new shopUdsOrderModel();
        $this->order_log_model = new shopOrderLogModel();
    }

    /** Геттер для скрытых свойств объекта (класса) */
    public function __get($name)
    {
        return $this->$name ?? null;
    }

    /**
     * ------------------------------------------------------------------
     * -- Раздел работы с сессией ---------------------------------------
     * ------------------------------------------------------------------
     */

    /** Название адреса хранилища сессии в Webasyst */
    const SESSION_VAR = 'shop/uds';

    /** Полная очистка сессии */
    static public function sessionClearAll()
    {
        wa()->getStorage()->remove(self::SESSION_VAR);
    }

    /** Запись параметра сессии */
    static public function sessionSet($key, $value)
    {
        $session = wa()->getStorage()->read(self::SESSION_VAR);
        if (!$session) {
            $session = [];
        }

        $session[$key] = $value;

        wa()->getStorage()->write(self::SESSION_VAR, $session);
    }

    /** Очистка параметра сессии */
    static public function sessionRemove($key)
    {
        $session = wa()->getStorage()->read(self::SESSION_VAR);

        if (isset($session[$key])) {
            unset($session[$key]);
        }

        wa()->getStorage()->write(self::SESSION_VAR, $session);
    }

    /** Чтение параметра сессии */
    static public function sessionGet($key)
    {
        return wa()->getStorage()->read(self::SESSION_VAR);
    }

    /**
     * Чтение или запись всей сессии
     * @param boolean|null|array $value
     * @return mixed|void|null
     */
    static public function session($value = false)
    {
        if ($value === false) {
            // Чтение
            return wa()->getStorage()->read(self::SESSION_VAR);
        } elseif ($value === null) {
            // Очистка
            wa()->getStorage()->remove(self::SESSION_VAR);
        } else {
            // Запись
            wa()->getStorage()->write(self::SESSION_VAR, $value);
        }
    }

    /**
     * ------------------------------------------------------------------
     * -- Раздел работы с Логом UDS -------------------------------------
     * ------------------------------------------------------------------
     */
    static public $log_enabled = true;

    static public $log_step = 0;
    static public $log_dir = 'nazarov/uds';
    static public $log_date = null;
    static public $log_hour = null;
    static public $log_datetime = null;

    static public function logBase($params)
    {
        if (self::$log_enabled !== true) {
            return false;
        }

        if (self::$log_date === null) {
            self::$log_date = date('Y-m-d');
        }
        if (self::$log_hour === null) {
            self::$log_hour = date('H');
        }
        if (self::$log_datetime === null) {
            self::$log_datetime = date('Y-m-d H:i:s');
        }
        $file = self::$log_dir . '/' . self::$log_date . '/' . self::$log_hour . '.log';
        $file_about_datetime = self::$log_dir . '/' . self::$log_date . '/' . self::$log_hour . '/' . self::$log_datetime . '.log';

        if (self::$log_step === 0) {
            $message = '------------------------------------------------------------';
            $text = $message . PHP_EOL . self::$log_step . ': ( НОВЫЙ ЗАПРОС СКРИПТА PHP и ПЛАГИНА ) ' . PHP_EOL . $message. PHP_EOL . $message;
            waLog::log($text, $file);
            waLog::log($text, $file_about_datetime);
        }

        self::$log_step = self::$log_step + 1;

        $trace = debug_backtrace();

        $caller_1 = $trace[1];
        $caller_2 = $trace[2];

        $caller_file_line = 'File: ' . $caller_1['file'] . ' Line: ' . $caller_1['line'];
        $caller_method_class = $caller_2['function'] . '()' . (isset($caller_2['class']) ? ' in ' . $caller_2['class'] . '::class' : '');

        $dump_text = isset($params['data']) ? ' - DUMP ' : '';

        $dump_data = false;
        if (isset($params['data'])) {
            if (!$params['wa_dump']) {
                $dump_data =  PHP_EOL . json_encode($params['data'],JSON_PRETTY_PRINT);
            }
        }


        $text = self::$log_step . ': (' . $caller_method_class . ')' . $dump_text
            . PHP_EOL . $caller_file_line
            . (!empty($params['message']) ? PHP_EOL . $params['message'] : '')
            . ( $dump_data ?? '')
        ;

        waLog::log($text, $file);
        waLog::log($text, $file_about_datetime);
        if (isset($params['data'])) {
            if ($params['wa_dump']) {
                waLog::dump($params['data'], $file);
                waLog::dump($params['data'], $file_about_datetime);
            }
        }
    }

    static public function log($message)
    {
        self::logBase(
            [
                'message' => $message,
            ]
        );
    }

    static public function dump($data, $message = '', $wa_dump = false)
    {
        self::logBase(
            [
                'data' => $data,
                'message' => $message,
                'wa_dump' => $wa_dump,
            ]
        );
    }


    /**
     * ------------------------------------------------------------------
     * -- Раздел работы с UDS API ---------------------------------------
     * ------------------------------------------------------------------
     */
    protected function returnError($message)
    {
        return [
            'status' => 'fail',
            'message' => $message,
        ];
    }

    protected function returnSuccess($data)
    {
        return [
            'status' => 'ok',
            'data' => $data,
        ];
    }

    /** Базовая функция поиска Клиента UDS по идентификатору (код/телефон) и фиксации данных в хранилище сессии */
    public function customerFind($identifier, $identifier_type)
    {
        // self::log('Step');

        $send_identifier = $identifier;
        if ($identifier_type == 'phone') {
            $send_identifier = str_replace("+", "%2B", $identifier);
        }

        // self::log('Step');

        $cart_total = shopUdsTotal::cartTotal();

        // self::log('Step');

        if (!$cart_total) {
            return $this->returnError('Server Error! Проблемы пересчета суммы корзины.');
        }

        // self::log('Step');

        $response = $this->api->customerFind(
            $send_identifier,
            $identifier_type,
            $cart_total['total'],
            $cart_total['skip_loyalty_total'],
        );

        // self::log('Step');

        if (!$response) {
            return $this->returnError('Remote UDS Server Error!');
        }

        // self::log('Step');

        $not_found_message = $this->pluginSettings['not_found_message_' . $identifier_type];
        $not_participant_message = $this->pluginSettings['not_participant_message'];
        if (isset($this->pluginSettings['not_participant_qr'])) {
            $qr_company = '/wa-data/public/shop/plugins/uds/' . $this->pluginSettings['not_participant_qr'];
            $not_participant_message = str_replace("[img-link]", $qr_company, $not_participant_message);
        }
        // self::log('Step');

        if ($response['status']['code'] == 404) {
            return $this->returnError($not_found_message);
        }

        // self::log('Step');

        if ($response['status']['code'] != 200) {
            return $this->returnError($response['status']['code'] . ': ' . $response['status']['phrase']);
        }

        // self::log('Step');

        $content = $response['content'];
        $is_participant = false;
        if ($content['user']['participant']['id']) {
            $is_participant = true;
        }
        // self::log('Step');
        // self::dump($this->pluginSettings, 'Настройки плагина');

        if (!$is_participant) {
            if ($this->pluginSettings['only_participants']) {
                return $this->returnError($not_participant_message);
            }
        }

        // self::dump($content, 'Результаты ответа на поиск клиента в API UDS');

        // self::log('Step');
        $data = [
            'uds_participant' => $content,
            'user' => [
                'uid' => $content['user']['uid'] ?? '0',
                'id' => $content['user']['participant']['id'] ?? '0',
                'name' => $content['user']['displayName'] ?? 'No Name',
                'points' => $content['user']['participant']['points'] ?? 0,
                'discount_rate' => $content['user']['participant']['DiscountRate'] ?? 0,
                'max_points' => $content['purchase']['maxPoints'] ?? 0,
                'cashback' => $content['user']['participant']['cashbackRate'],
                'phone' => $content['user']['phone'] ?? '',
            ],
            'identifier' => [
                'type' => $identifier_type,
                'value' => $identifier,
            ]
        ];
        // self::log('Step');

        return $this->returnSuccess($data);
    }

    /** Вознаграждение бонусными баллами за проведение покупки в магазине */
    public function operationReward($order_id)
    {
        shopUdsHelper::dump($order_id, 'Начало операции возврата');
//        $order = $this->order_model->getById($order_id);
        $uds_order = $this->uds_order_model->getByField('order_id', $order_id);
        //TODO: Вознаграждение бонусными баллами за проведение покупки в магазине
        $comment = 'Вознаграждение за покупку в интернет-магазине по заказу №' . $order_id . '(' . date('d.m.Y H:i:s') . ')';
        $participant_id = $uds_order['uds_user_id'];
        $points_for_reward = $uds_order['points_for_reward'];

        shopUdsHelper::dump([
            'participant_id' => $participant_id,
            'points_for_reward' => $points_for_reward,
            'comment' => $comment,

        ], 'Параметры перед вызовом API');

        $response = $this->api->operationReward($participant_id, $points_for_reward, $comment);

        shopUdsHelper::dump($response, 'Ответ от API - operationReward');

        if ($response['status']['code'] != 202) {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
            );
            $this->order_log_model->insert($data_log);
        } else {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: green;">UDS - Вознаграждение было успешно начислено для Транзакции: ' . $uds_order['uds_operation_purchase_id'] . '</code>',
            );
            $this->order_log_model->insert($data_log);
            $this->uds_order_model->updateById($uds_order['id'], [
                'rewarded' => 1
            ]);
        }
    }

}
