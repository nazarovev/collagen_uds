<?php

class shopUdsHelperNew
{
    /** @var mixed настройки плагина */
    protected array $pluginSettings;
    /** @var mixed Инициализированный объект класса для работы с API UDS */
    protected shopUdsHelperApi $api;
    /** @var array настройки компании в системе UDS */
    protected array $udsCompanySettings;

    /** @var shopOrderModel Модель заказов */
    protected shopOrderModel $order_model;
    /** @var shopUdsOrdersNewModel Модель заказов UDS */
    protected shopUdsOrdersNewModel $uds_order_model;
    /** @var shopOrderLogModel Модель лога заказов */
    protected shopOrderLogModel $order_log_model;


    /** Конструктор */
    public function __construct()
    {
        $plugin = wa('shop')->getPlugin('uds');
        $this->pluginSettings = $plugin->getSettings();

        $this->api = new shopUdsHelperApi($this->pluginSettings['api_key'], $this->pluginSettings['company_id']);
        $this->udsCompanySettings = $this->api->settings();

        $this->order_model = new shopOrderModel();
        $this->uds_order_model = new shopUdsOrdersNewModel();
        $this->order_log_model = new shopOrderLogModel();
    }

    /** Вспомогательная функция - Формирование ответе Success */
    protected function returnSuccess($message = 'OK', $data = null)
    {
        return [
            'status' => 'success',
            'message' => $message,
            'data' => $data,
        ];
    }

    /** Вспомогательная функция - Формирование ответе Error */
    protected function returnError($message = 'Error!', $data = null)
    {
        return [
            'status' => 'error',
            'message' => $message,
            'data' => null,
        ];
    }

    /** Базовая функция поиска Клиента UDS по идентификатору (код/телефон) и фиксации данных в хранилище сессии */
    protected function customerFind($identifier, $identifier_type)
    {
        if ($identifier_type == 'phone') {
            $identifier = str_replace("+", "%2b", $identifier);
        }

        $cart_total = shopUdsHelperCartTotal::calc();
        if (!$cart_total) {
            return $this->returnError('Server Error!');
        }

        $response = $this->api->customerFind(
            $identifier,
            $identifier_type,
            $cart_total->getTotal(),
            $cart_total->getSkipLoyaltyTotal(),
        );

        if (!$response) {
            return $this->returnError('Remote UDS Server Error!');
        }

        $not_found_message = '';
        if ($identifier_type == 'phone') {
            $not_found_message = $this->pluginSettings['not_found_message_phone'];
        } elseif ($identifier_type == 'code') {
            $not_found_message = $this->pluginSettings['not_found_message_code'];
        }

        $not_participant_message = $this->pluginSettings['not_participant_message'];

        if (isset($this->pluginSettings['not_participant_qr'])) {
            $qr_company = '/wa-data/public/shop/plugins/uds/' . $this->pluginSettings['not_participant_qr'];
            $not_participant_message = str_replace("[img-link]", $qr_company, $not_participant_message);
        }

        if ($response['status']['code'] == 404) {
            return $this->returnError($not_found_message);
        }

        if ($response['status']['code'] != 200) {
            return $this->returnError($response['status']['code'] . ': ' . $response['status']['phrase']);
        }


        $content = $response['content'];
        $is_participant = false;
        if ($content['user']['participant']['id']) {
            $is_participant = true;
        }

        waLog::dump($this->pluginSettings, 'uds_dump.log');
        if (!$is_participant) {
            if ($this->pluginSettings['only_participants']) {
                return $this->returnError($not_participant_message);
            }
        }

        waLog::dump($content, 'uds_dump.log');

        // полученные ответы от UDS
        $uds_user_info = [
            'uds_user_uid' => $content['user']['uid'] ?? '0',
            'uds_user_name' => $content['user']['displayName'] ?? 'No Name',
            'uds_user_points' => $content['user']['participant']['points'] ?? 0,
            'uds_user_discount_rate' => $content['user']['participant']['DiscountRate'] ?? 0,
            'uds_user_max_points' => $content['purchase']['maxPoints'] ?? 0,
            'uds_user_cashback' => $content['user']['participant']['cashbackRate'],
            'uds_user_phone' => $content['user']['phone'] ?? '',

            'uds_discount_identifier_type' => $identifier_type,
            'uds_discount_identifier' => $identifier,
        ];

        $result = [
            'user' => $uds_user_info,
            'reload' => false,
        ];

        if ($uds_user_info['uds_user_discount_rate'] > 0 && $identifier_type == 'code') {
            wa()->getStorage()->set('shop/uds/discount', $uds_user_info['uds_user_discount_rate']);
            $result['reload'] = true;
        }
        // В хранилище shop/udsdiscount/user устанавливается информация о клиенте $user_info
        wa()->getStorage()->set('shop/uds/user', $uds_user_info);

        // Возвращается массив $data_return с информацией о статусе запроса и данных о клиенте
        return $this->returnSuccess('return uds_user_info', $result);
    }

    /** Поиск клиента UDS по телефону и фиксация данных в сессии */
    public function customerFindByPhone($phone)
    {
        $phone = str_replace("+", "%2b", $phone);

        return $this->customerFind($phone, 'phone');
    }

    /** Поиск клиента UDS по коду, и фиксация данных в сессии */
    public function customerFindByCode($code)
    {
        return $this->customerFind($code, 'code');
    }

    /** Возвращает настройки плагина */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /** Возвращает настройки компниии в UDS */
    public function getUdsCompanySettings()
    {
        return $this->udsCompanySettings;
    }

    // /** Cоздание покупки со списанием баллов, если по коду, или без списания, с будущим начислением через Reward */
    // public function operationPurchase($order_id)
    // {
    //     $uds_order = $this->uds_order_model->getByField('order_id', $order_id);
    //
    //     $order_totals = shopUdsHelperOrderTotal::calc($order_id);
    //     if (!$order_totals) {
    //         $this->order_log_model->insert([
    //             'order_id' => $order_id,
    //             'action_id' => '',
    //             'datetime' => date('Y-m-d H:i:s'),
    //             'before_state_id' => '',
    //             'after_state_id' => '',
    //             'text' => '<code style="color: red;">ERROR (UDS) (Регистрация покупки в UDS): Нет информации о товарах внутри заказа, возможна внутренняя ошибка</code>',
    //         ]);
    //     }
    //
    //     $response = null;
    //     if ($uds_order['identifier_type'] == 'phone') {
    //         $response = $this->api->operationCreateByPhone(
    //             $uds_order['identifier'],
    //             $order_totals->getTotal(),
    //             $order_totals->getTotal(),
    //             0,
    //             $order_id,
    //             $order_totals->getTotal(),
    //         );
    //     } elseif ($uds_order['identifier_type'] == 'code') {
    //         $response = $this->api->operationCreateByCode(
    //             $uds_order['identifier'],
    //             $order_totals->getTotal(),
    //             (float)($order_totals->getTotal() - $uds_order['points']),
    //             (float)$uds_order['points'],
    //             (string)$order_id,
    //             $order_totals->getTotal(),
    //         );
    //     }
    //
    //     if (!$response) {
    //         waLog::dump('Response fail', 'uds_dump.log');
    //     }
    //
    //     waLog::dump($response, 'uds_dump.log');
    //
    //     // Если в ответе содержится ошибка, то информация об ошибке добавляется в логи заказа.
    //     if ($response['status']['code'] != 200) {
    //         // Add to order logs
    //         $data_log = array(
    //             'order_id' => $order_id,
    //             'action_id' => '',
    //             'datetime' => date('Y-m-d H:i:s'),
    //             'before_state_id' => '',
    //             'after_state_id' => '',
    //             'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
    //         );
    //         $this->order_log_model->insert($data_log);
    //         // Если в ответе нет ошибки, то полученный идентификатор используется для обновления данных в модели заказов. Обновление происходит в зависимости от переданного в запросе параметра $action.
    //     } else {
    //         $purchase_id = $response['content']['id'];
    //
    //         $this->uds_order_model->updateById($uds_order['id'],
    //             [
    //                 'points_for_reward' => $order_totals->getTotalForReward() * ( $uds_order['cashback_for_reward'] / 100 ),
    //                 'purchased' => 1,
    //                 'uds_operation_purchase_id' => $purchase_id,
    //             ]
    //         );
    //
    //     }
    // }

    /** Отмена покупки, если заказ в магазине был отменен */
    public function operationRefund($order_id, $action_id = '', $before_state_id = '', $after_state_id = '', $refund_amount = null)
    {
        $uds_order = $this->uds_order_model->getByField('order_id', $order_id);


        if (!$uds_order['uds_operation_purchase_id']) {
            return;
        }

        $response = $this->api->operationRefund($uds_order['uds_operation_purchase_id'], $refund_amount);

        if ($response['status']['code'] != 200) {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => $action_id,
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => $before_state_id,
                'after_state_id' => $after_state_id,
                'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
            );
            $this->order_log_model->insert($data_log);
        } else {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => $action_id,
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => $before_state_id,
                'after_state_id' => $after_state_id,
                'text' => '<code style="color: green;">UDS - Возврат был успешно произведен для Транзакции: ' . $uds_order['uds_operation_purchase_id'] . '</code>',
            );
            $this->order_log_model->insert($data_log);
            $this->uds_order_model->updateById($uds_order['id'], [
                'refunded' => 1
            ]);
        }

    }

    /** Вознаграждение бонусными баллами за проведение покупки в магазине */
    public function operationReward($order_id)
    {
//        $order = $this->order_model->getById($order_id);


        $uds_order = $this->uds_order_model->getByField('order_id', $order_id);
        //TODO: Вознаграждение бонусными баллами за проведение покупки в магазине
        $comment = 'Вознаграждение за покупку в интернет-магазине по заказу №' . $order_id . '(' . date('d.m.Y H:i:s') . ')';
        $participant_id = $uds_order['uds_uid'];
        $points_for_reward = $uds_order['points_for_reward'];

        $response = $this->api->operationReward($participant_id, $points_for_reward, $comment);

        if ($response['status']['code'] != 200) {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: red;">UDS - ' . print_r($response, true) . '</code>',
            );
            $this->order_log_model->insert($data_log);
        } else {
            $data_log = array(
                'order_id' => $order_id,
                'action_id' => '',
                'datetime' => date('Y-m-d H:i:s'),
                'before_state_id' => '',
                'after_state_id' => '',
                'text' => '<code style="color: green;">UDS - Вознаграждение было успешно начислено для Транзакции: ' . $uds_order['uds_operation_purchase_id'] . '</code>',
            );
            $this->order_log_model->insert($data_log);
            $this->uds_order_model->updateById($uds_order['id'], [
                'refunded' => 1
            ]);
        }
    }


}
